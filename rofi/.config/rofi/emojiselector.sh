#!/bin/bash

if [ -z $@ ]; then
    cat ~/.config/rofi/emoji
else
    emoji=$(echo "$@" | sed "s/ .*//")
    echo "$emoji" | xclip -r
    echo "$emoji" | xclip -r -selection clipboard
    killall rofi
fi
