# dotfiles

## Adopting settings

`stow --adopt <folder>` in this directory will move data from `~/<folder[/subdirs/file]>` to `<folder[/subdirs/file]>` if folders/filenames exists in this repo.

## ZSH

ZSH configs must be moved to `~/.config/zsh` via env var `ZDOTDIR="$HOME/.config/zsh"` (**double quotation marks**) (in eg. `/etc/zsh/zshenv` or `/etc/zshenv`).

Do not install oh-my-zsh, [antigen](https://github.com/zsh-users/antigen) is used instead (easy to install from AUR).

## sudo

My sudo config (`/etc/sudoers.d/custom` (no extensions), set file perm to 0644 or similarily restrictive):

```
# Remove time limit on password prompts (seconds)
Defaults passwd_timeout=0

# Time before sudo reprompt (minutes)
Defaults timestamp_timeout=30

# Change prompt text
Defaults passprompt="🔒 Password for %p: "

# Commands where not to require password
Cmd_Alias PKGCMDS = /usr/bin/pacman -Syu,/usr/bin/pacman -Syuu,/usr/bin/pacman -Su,/usr/bin/pacman -Ss
Cmd_Alias WIFICMDS = /usr/bin/ip link set wlp5s0 down,/usr/bin/ip link set wlp5s0 up
Cmd_Alias VERACMDS = /usr/bin/veracrypt -d

# Allow group wheel to use sudo, and no pw requirement for some cmds
%wheel ALL=(ALL) ALL
%wheel ALL=(ALL) NOPASSWD: PKGCMDS
%wheel ALL=(ALL) NOPASSWD: WIFICMDS
%wheel ALL=(ALL) NOPASSWD: VERACMDS
```

## bin

If NodeJS is not installed at `/usr/bin/node`, some script headers has to be changed to the correct node installation path. (Or use `/bin/env node`...)

## Other notes

### X11

#### Swedish keyboard layout

`/etc/X11/xorg.conf.d/20-keyboard.conf`

```
Section "InputClass"
  Identifier "keyboard"
  MatchIsKeyboard "yes"
  Option "XkbLayout" "se"
EndSection
```

#### Lower default mouse speed

(Best way I found for my SteelSeries Rival 650, other mice may have better options for lowering the insanely high default speed)

`/etc/X11/xorg.conf.d/90-libinput.conf`

```
Section "InputClass"
  Identifier "SteelSeries Rival 650"
  MatchDriver "libinput"
  MatchProduct "SteelSeries Rival 650"
  Option "Accel Speed" "-0.8"
EndSection
```

#### Enable tearfree option for AMD gpu or Radeon gpu

(`amdgpu` should be prefered but `radeon` may be needed for older gpus)

`/etc/X11/xorg.conf.d/99-tearfree.conf`

```
Section "Device"
  Identifier "amdgpu"
  Driver "amdgpu"
  Option "TearFree" "true"
EndSection
```

or

```
Section "Device"
	Identifier "Radeon"
	Driver "radeon"
	Option "TearFree" "on"
EndSection
```

### Network drives

Setup password protected NAS connection to only open when used.

My setup sets up two different directories on my NAS, the private (/home) and the public (/alla).

`/etc/fstab`

```
//<hostname>/home /mnt/data/home cifs cred=/home/<user>/.config/homecredentials,vers=3,uid=1000,gid=1000,forceuid,forcegid,iocharset=utf8,noauto,x-systemd.automount,x-systemd.device-timeout=10,nofail,_netdev 0 0
//<hostname>/alla /mnt/data/alla cifs cred=/home/<user>/.config/homecredentials,vers=3,uid=1000,gid=1000,forceuid,forcegid,iocharset=utf8,noauto,x-systemd.automount,x-systemd.device-timeout=10,nofail,_netdev 0 0
```

`.config/homecredentials` is a protected file with the following format:

```
username=<user>
password=<pass>
domain=
```

(yes, `domain` is empty)

### LightDM

I use `lightdm` and `lightdm-webkit2-greeter` with my [minimal-lightdm-theme](https://gitlab.com/josefandersson/minimal-lightdm-theme).

1. Install packages
2. Follow setup steps in theme repo (see [README.md](https://gitlab.com/josefandersson/minimal-lightdm-theme/-/blob/master/README.md))

- Set `greeter-setup-script=xset r rate 195 30` in `/etc/lightdm/lightdm.conf` for proper keyboard speed

### Packages

Packages that I should wish to install.

#### Official repos

`pacman -S autoconf automake bspwn chromium cifs-utils cups discord dunst entr exa feh ffmpegthumbnailer firefox flameshot fzf gcc gimp git gnome-keyring gvfs gvfs-mtp gvfs-smb gvfs-gphoto2 gvfs-afc htop httpie hunspell-en_US intel-ucode intellij-idea-community-edition jdk-openjdk jq jre-openjdk kitty libreoffice-fresh libreoffice-fresh-en-gb libreoffice-fresh-sv lightdm-webkit2-greeter lvm2 lxappearance make man-db man-pages mesa-vdpau mpc mpd mpv mupdf nano ncmpcpp neofetch newsboat nfs-utils nitrogen nmap nodejs noto-fonts noto-fonts-cjk noto-fonts-emoji ntfs-3g numlockx openbox otf-font-awesome pacman-contrib patch pavucontrol picom playerctl python2 raw-thumbnailer redshift rofi rsync sl smartmontools sqlitebrowser stow sudo sxhkd telegram-desktop thunar thunar-media-tags-plugin thunar-volman transmission-gtk ttf-fira-code ttf-fira-mono ttf-fira-sans ttf-jetbrains-mono ttf-joypixels tumbler veracrypt viewnior virtualbox vlc vulkan-radeon wget which xarchiver xautolock xclip xdo xdotool xf86-video-ati xorg-xev xorg-xinput xorg-xwininfo xprintidle xterm youtube-dl zsh`

#### User repos

`trizen -S antigen brave-bin betterdiscord-installer hunspell-sv i3lock-color-git libinput-multiplier libxft-bgra-git minecraft-launcher otf-takao polybar python-pywal r-tinytex themix-full-git ttf-ms-fonts typora webm xmacro xtitle`
