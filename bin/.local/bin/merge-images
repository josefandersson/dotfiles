#!/bin/bash

# Merge image files
# Reads file name/paths from stdin until an empty line is read
# Can merge horizontally, vertically and into a grid

# merge-images [type=grid] [image1] [image2] ...
# types: (g)rid, (h)orizontal, (v)ertical

# Make sure type is good
case $1 in
    grid|horizontal|vertical|g|h|v|'') ;;
    *) echo "bad type" ; exit 1 ;;
esac

fns=""
first=""
count=0

if [ $# -gt 1 ]; then
    # Read filenames from argument list
    count=$#
    first=$2
    fns=" ${@:2}"
else
    # Read filenames from stdin
    while read fn ; do
        [ -z "$fn" ] && break
        fn=$(echo $fn | sed s/\'\"//)
        [ -z "$first" ] && first="$fn"
        fns="$fns $fn"
        ((count++))
    done
fi

[ $count -lt 2 ] && echo "Not enough files" && exit 1

echo Files:$fns
read -r -p "Proceed? [y/N] " response
case "$response" in
    [yY]) ;;
    *) exit 1 ;;
esac

fstdir="$(dirname "$first")"
fstfn="$(basename "$first")"
fstext="${fstfn##*.}"
[ $fstext != "png" ] && echo "$fns" | grep -qE "\.png( |$)" && fstext="png"
fstfn="${fstfn%.*}"
deffn="$fstdir/${fstfn}-merged.$fstext"

read -r -p "Output filename ($deffn): " outputfn
[ -z "$outputfn" ] && outputfn=$deffn

# Verify overwrite
if [ -f "$outputfn" ]; then
    read -r -p "Output file already exists, overwrite? [y/N] " response
    case "$response" in
        [yY]) ;;
        *) exit 1 ;;
    esac
fi

# Determine type argument
case $1 in
    grid|g|'') tile="xx" ;;
    vertical|v) tile="x$count" ;;
    horizontal|h) tile="${count}x" ;;
esac

echo "Running: montage$fns -geometry +0+0 -tile $tile -background transparent \"$outputfn\""
montage$fns -geometry +0+0 -tile "$tile" -background transparent "$outputfn"