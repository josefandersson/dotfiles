#!/usr/bin/nodejs

function printHelpMessage() {
    console.log('jcalendar - v1');
    console.log('Usage: jcalendar [flags...]');
    console.log('');
    console.log('  -h                   Print help message');
    console.log('  -i, --interactive    Run in interactive mode');
    console.log('  -s, --start=DATE     Start date');
    console.log('  -e, --end=DATE       End date');
    console.log('  -p, --period=PERIOD  Date period');
    console.log('      --no-week        Do not order dates by week');
    console.log('      --no-color       Disable colors');
    console.log('');
    console.log('DATE format');
    console.log('  YYYY-MM-DD or YYYY-MM or YYYY.');
    console.log('  If year is YY it is assumed to be 20YY.');
    console.log('PERIOD format');
    console.log('  A string of any or all of NNy, NNm, NNw,');
    console.log('  NNd, where NN is a number. Eg:');
    console.log('  --period=1y5m3d for one year, five months');
    console.log('  and three days. If -s or -e are speficied');
    console.log('  period will be calculated before or after');
    console.log('  those dates, otherwise period will be before');
    console.log('  current date.');
    process.exit(0);
}

function printError(errorMsg) {
    console.log(errorMsg);
    console.log('Use jcalendar -h for help message');
    process.exit(1);
}

const opts = {
    interactive: false,
    noWeek: false,
    noColor: false,
    sundayFirst: false,
    start: null,
    end: null,
    period: [1,0,0,0],
    spacingYear: '-',
    spacingMonth: '=',
    months: ['JANUARY','FEBRUARY','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOBER','NOVEMBER','DECEMBER'],
    days: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
};

for (let i = 2; i < process.argv.length; i++) {
    const arg = process.argv[i];
    const r = /^(-{0,2})(\S+?)(?:=(.*))?$/.exec(arg);
    if (r === null) {
        printError(`Unknown argument: ${arg}`);
    } else {
        if (r[1] === '-') {
            const flags = new Set(r[2].split(''));
            for (let f of flags) {
                switch (f) {
                    case 'i':
                        opts.interactive = true;
                        break;
                    case 's':
                        if (process.argv.length - 1 === i) printError('No DATE was provided to -s flag');
                        opts.start = new Date(process.argv[++i]);
                        if (isNaN(opts.start.getTime())) printError('Invalid DATE was provided to -s flag');
                        break;
                    case 'e':
                        if (process.argv.length - 1 === i) printError('No DATE was provided to -e flag');
                        opts.end = new Date(process.argv[++i]);
                        if (isNaN(opts.end.getTime())) printError('Invalid DATE was provided to -e flag');
                        break;
                    case 'p':
                        if (process.argv.length - 1 === i) printError('No PERIOD was provided to -p flag');
                        const str = process.argv[++i];
                        if (!/^(\d+[ymwd])+$/.test(str)) printError('Invalid PERIOD was provided to -p flag');
                        opts.period = [/(\d)+y/, /(\d)+m/, /(\d)+w/, /(\d)+d/].map(r => str.match(r)).map(r => r == null ? 0 : +r[1]);
                        if (opts.period.every(n => n === 0)) printError('Invalid PERIOD was provided to -p flag, cannot be zero');
                        break;
                    case '?':
                    case 'h':
                        printHelpMessage();
                        break;
                    default:
                        printError(`Unknown flag: ${f}`);
                }
            }
        } else if (r[1] === '--') {
            switch (r[2]) {
                case 'interactive':
                    opts.interactive = true;
                    break;
                case 'no-week':
                    opts.noWeek = true;
                    break;
                case 'start':
                    if (!r[3]) printError('No DATE was provided to --start flag');
                    opts.start = new Date(r[3]);
                    if (isNaN(opts.start.getTime())) printError('Invalid DATE was provided to --start flag');
                    break;
                case 'end':
                    if (!r[3]) printError('No DATE was provided to --end flag');
                    opts.end = new Date(r[3]);
                    if (isNaN(opts.end.getTime())) printError('Invalid DATE was provided to --end flag');
                    break;
                case 'period':
                    if (!r[3]) printError('No PERIOD was provided to --period flag');
                    if (!/^(\d+[ymwd])+$/.test(r[3])) printError('Invalid PERIOD was provided to --period flag');
                    opts.period = [/(\d)+y/, /(\d)+m/, /(\d)+w/, /(\d)+d/].map(r => r[3].match(r)).map(r => r == null ? 0 : +r[1]);
                    if (opts.period.every(n => n === 0)) printError('PERIOD provided to -p flag cannot be zero');
                    break;
                default:
                    printError(`Unknown flag: ${r[2]}`);
            }
        } else printError(`Unknown argument: ${arg}`);
    }
}

function simplifyDate(date) {
    date.setUTCHours(0);
    date.setUTCMinutes(0);
    date.setUTCSeconds(0);
    date.setUTCMilliseconds(0);
    return date;
}

function cycleDay(date) {
    if (opts.sundayFirst) return date;
    return (date + 6) % 7;
}

function printCalendar() {
    const [w, h] = process.stdout.getWindowSize();

    if (opts.start) simplifyDate(opts.start);
    if (opts.end) simplifyDate(opts.end);
        
    if (!opts.end) {
        if (opts.start) {
            opts.end = new Date(opts.start);
            opts.end.setFullYear(opts.start.getFullYear() + opts.period[0]);
            opts.end.setMonth(opts.start.getMonth() + opts.period[1]);
            opts.end.getTime(opts.end.getTime() + opts.period[2] * 604800000 + opts.period[3] * 86400000);
        } else {
            opts.end = simplifyDate(new Date());
            opts.end.setMonth(opts.end.getMonth() + 6);
        }
    }

    if (!opts.start) {
        opts.start = new Date();
        opts.start.setFullYear(opts.end.getFullYear() - opts.period[0]);
        opts.start.setMonth(opts.end.getMonth() - opts.period[1]);
        opts.start.setTime(opts.end.getTime() - opts.period[2] * 604800000 - opts.period[3] * 86400000);
    }
    
    const start = opts.start;
    const end = opts.end;

    simplifyDate(start);
    simplifyDate(end);

    if (end.getTime() < start.getTime()) printError("Start DATE must be before end DATE");

    let years = [{ year:start.getFullYear(), months:[{ month:start.getMonth(), dates:[] }] }];
    let yi = 0, mi = 0;
    if (!opts.noWeek)
        for (let i = cycleDay(start.getDay()), date = new Date(start); 0 < i; i--) {
            date.setDate(date.getDate() - 1);
            years[yi].months[mi].dates[i-1] = { date:date.getDate(), f:true };
        }
    let previous, current = opts.start;
    while (1) {
        if (previous && previous.getMonth() !== current.getMonth()) {
            let previousMonth = years[yi].months[mi];
            if (previous && previous.getFullYear() !== current.getFullYear())
                years[++yi] = { year:current.getFullYear(), months:[{ month:mi=0, dates:[] }] };
            else
                years[yi].months[++mi] = { month:current.getMonth(), dates:[] };
            if (!opts.noWeek) {
                const day = cycleDay(current.getDay());
                const len = previousMonth.dates.length;
                for (let i = day; 0 < i; i--) {
                    years[yi].months[mi].dates[i-1] = { date:previousMonth.dates[len-(day-i)-1].date, f:true };
                }
            }
        }

        years[yi].months[mi].dates.push({ date:current.getDate(), f:false });
        
        previous = new Date(current);
        current.setDate(current.getDate() + 1);

        if (current.getTime() >= end.getTime()) break;
    }
    
    const monthsPerLine = Math.max(1, (w + 2) / 24);
    const calendarWidth = monthsPerLine * 24 - 2;
    for (let yi = 0; yi < years.length; yi++) {
        const year = years[yi];
        let spacing = opts.spacingYear.repeat((calendarWidth - 8) / 2);
        process.stdout.write(`|${spacing} ${year.year} ${spacing}|`);
        process.stdout.moveCursor(-calendarWidth, 1);

        let y = 0;
        for (let mi = 0; mi < year.months.length; mi++) {
            const month = year.months[mi];
            const monthStr = opts.months[month.month];
            let spacing = opts.spacingMonth.repeat(Math.floor(12 - ((monthStr.length + 6) / 2)))
            let spacingRight = spacing + (monthStr.length % 2 === 0 ? '' : opts.spacingMonth);
            process.stdout.write(`|${spacing} ${monthStr} ${spacingRight}|`);
            process.stdout.moveCursor(-24, 1);
            y++;
            
            if (!opts.noWeek) {
                process.stdout.write(`|${opts.days.join('|')}|`);
                process.stdout.moveCursor(-24, 1);
                y++;
            }

            let str = '';
            for (let di = 0; di < month.dates.length; di++) {
                const date = month.dates[di];
                str += '|';
                if (!opts.noColor)
                    str += date.f ? '\x1b[38;5;65m' : '\x1b[38;5;10m';
                str += `${date.date<10?' ':''}${date.date}`;
                if (!opts.noColor)
                    str += '\x1b[0m';
                if (di % 7 === 6) {
                    console.log(`${str}|`);
                    y++;
                    str = '';
                }
            }
            if (str.length) {
                console.log(`${str}|`);
                y++;
            }
            
            
            if (mi % monthsPerLine === monthsPerLine - 1)
                process.stdout.moveCursor(-calendarWidth, 5);
            else
                process.stdout.moveCursor(24, -y);
            y = 0;
        }
    }

    // const monthsPerLine = Math.max(1, (w + 2) / 24);
    // const calendarWidth = monthsPerLine * 24 - 2;
    // let previous, current = opts.start;
    // while (1) {
    //     if (!previous || previous.getFullYear() !== current.getFullYear()) {
    //         const spacing = opts.spacingYear.repeat((calendarWidth - 8) / 2);
    //         console.log(`|${spacing}`, current.getFullYear(), `${spacing}|`);
    //     }

    //     if (!previous || previous.getMonth() !== current.getMonth()) {
    //         const month = opts.months[current.getMonth()];
    //         const spacing = opts.spacingMonth.repeat(Math.floor(24 - ((month.length + 2) / 2)))
    //         console.log(`|${spacing}`, month, `${spacing}|`);
    //     }

    //     console.log(current.getDate());
        
    //     previous = new Date(current);
    //     current.setDate(current.getDate() + 1);

    //     if (current.getTime() >= end.getTime()) break;
    // }
}

if (opts.interactive) {
    const readline = require('readline');
    const rl = readline.createInterface({ input:process.stdin, output:process.stdout });

    process.stdin.on('data', ev => {
        const keyCode = ev.readInt8();
        process.stdout.clearLine('left');
        process.stdout.moveCursor(-1, keyCode === 13 ? -1 : 0);
        
        if (keyCode === 113) process.exit(0);
    });
}

printCalendar();


