#!/bin/bash

# macro version 1.0, by Josef Andersson 2021 (github.com/josefandersson)
# Requires xmacro (download.sarine.nl/xmacro/Description.html)
# Macro directory: ~/.local/share/macros/

# Records and plays keyboard and mouse macros.
# Usage: macro [cmds...]
# Single commands: (not stackable, exits program)
#   help         Display this message.
#   list         List all recorded macros.
#   version      Display version info.
# Stackable commands: (order of commands is respected)
#   key <code>      Set key for stopping macro recording. (get code from eg. xev)
#   last            Play last recorded macro.
#   play <name>     Play macro with name <name>.
#   recnosave       Record a macro but don't save it with any name.
#   record <name>   Record and save a macro with name <name>.
#   rmdelays <name> Remove delays in macro with name <name>.
#   speed <n>       Set playback speed to <n>. (0.5 is double, 2 is half)
#   sleep <sec>     Sleep <sec> seconds before executing the next command.
#   verbose         Enable printing info messages.

# Print version and help messages
[ -z "$1" ] || [ "$1" = "version" ] && sed -n '3,5P' "$BASH_SOURCE" && exit
[ "$1" = "help" ] && sed -n '7,22P' "$BASH_SOURCE" && exit

# Ensure macro dir
dn="$HOME/.local/share/macros"
[ -d "$dn" ] || mkdir -p "$dn"

# Special options
[ "$1" = "list" ] && ls "$dn" -1 && exit

# Default values
speed=0.5
key=90
verbose=0

# Function for writing info message
# info <message...>
info() {
    [ "$verbose" = 1 ] && echo "$@"
}

# Function for playing macro
# playMacro [name]
playMacro() {
    [ -n "$1" ] && name="$1" || name="last"
    [ -f "$dn/$name" ] || (echo "No macro '$name'" && exit 1)
    info "Playing macro '$name'"
    res=$(cat "$dn/$name" | xmacroplay --speed "$speed" 2> >(grep error))
    [ -n "$res" ] && echo "Failed with error(s):"$'\n'"$res" && exit 1
    info "Done playing macro '$name'"
}

# Function for recording a macro, saves as name if name is provided
# recordMacro [name]
recordMacro() {
    info "Recording macro"
    script=$(xmacrorec2 -k "$key" 2> /dev/null)
    [ -z "$script" ] && echo "Nothing was recorded" && exit 1
    info "Done recording macro"
    echo "$script" > "$dn/last"
    [ -n "$1" ] && echo "$script" > "$dn/$1" && info "Saved macro '$1'"
}

# Function for removing delays in a macro, overwrites old version
# removeDelays <name>
removeDelays() {
    [ -f "$dn/$1" ] || (echo "No macro '$1'" && exit 1)
    # sed -E 's/Delay [0-9]+/Delay 50/' -i "$dn/$1"
    sed -E '/^Delay [0-9]+$/d' -i "$dn/$1"
    info "Removed delays from macro '$dn/$1'"
}

# Handle arguments
while [ "$#" -gt 0 ]; do
    case "$1" in
        key)
            shift
            if [ "$#" -gt 0 ]; then
                [[ "$1" =~ ^[0-9]+$ ]] || (echo "bad argument <code> for 'key'" && exit 1)
                key=$1
            else
                echo "missing argument <code> for 'key'"
                exit 1
            fi ;;

        last)
            playMacro
            ;;
            
        play)
            shift
            if [ "$#" -gt 0 ]; then
                [[ "$1" =~ / ]] && echo "bad argument <name> for 'play'" && exit 1
                playMacro "$1"
            else
                echo "missing argument <name> for 'play'"
                exit 1
            fi ;;

        recnosave)
            recordMacro
            ;;

        record)
            shift
            if [ "$#" -gt 0 ]; then
                [[ "$1" =~ / ]] && echo "bad argument <name> for 'record'" && exit 1
                recordMacro "$1"
            else
                echo "missing argument <name> for 'record'"
                exit 1
            fi ;;

        rmdelays)
            shift
            if [ "$#" -gt 0 ]; then
                [[ "$1" =~ / ]] && echo "bad argument <name> for 'rmdelays'" && exit 1
                removeDelays "$1"
            else
                echo "missing argument <name> for 'rmdelays'"
                exit 1
            fi ;;

        speed)
            shift
            if [ "$#" -gt 0 ]; then
                [[ "$1" =~ ^[0-9]*\.?[0-9]+$ ]] || (echo "bad argument <n> for 'speed'" && exit 1)
                speed="$1"
            else
                echo "missing argument <n> for 'speed'"
                exit 1
            fi ;;
        
        sleep)
            shift
            if [ "$#" -gt 0 ]; then
                [[ "$1" =~ ^[0-9]*\.?[0-9]+$ ]] || (echo "bad argument <sec> for 'sleep'" && exit 1)
                sleep "$1"
            else
                echo "missing argument <sec> for 'sleep'"
                exit 1
            fi ;;

        verbose)
            verbose=1
            ;;
    esac
    shift
done