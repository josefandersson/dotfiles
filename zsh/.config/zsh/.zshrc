# Load antigen
source /usr/share/zsh/share/antigen.zsh

antigen use oh-my-zsh

antigen bundle git
antigen bundle colored-man-pages
antigen bundle extract
antigen bundle fzf
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle colorize
antigen bundle jump
antigen bundle httpie
antigen bundle zsh-users/zsh-syntax-highlighting

antigen theme robbyrussell

# antigen apply
antigen apply

# Setup history
HISTSIZE=100000
SAVEHIST=100000
HISTFILE=$HOME/.cache/zsh_history

# Setup aliases
alias \
    xclipc="xclip -selection clipboard" \
    pwdc="pwd | xclipc" \
    cp="rsync -pogbr -hhh --backup-dir=/tmp/rsync -e /dev/null --info=progress2" \
    ls="exa" \
    la="exa -alF" \
    l="exa -lF" \
    lt="exa -aTF" \
    yt="youtube-dl" \
    ytx="youtube-dl -x" \
    uc="wal -i \"\$(cat ~/.config/nitrogen/bg-saved.cfg | grep file -m 1 | sed s/file=//g)\" -a 80 -n --saturate 1.0" \
    neofetcha="neofetch --kitty $HOME/.config/neofetch/avatar.png --size 240px" \
    igpp="cat | xargs -I {} sh -c \"curl -s https://www.instagram.com/{}/?__a=1 | jq -r '.graphql.user.profile_pic_url_hd' | tee /dev/fd/2 | xclip -selection clipboard && echo 'copied for {}' || echo 'failed to fetch {}'\"" \
    feh="feh -. -g 1900x1050+5+15 -B black --auto-rotate --start-at" \
\
    S="sudo pacman -S" \
    Su="sudo pacman -Su" \
    Syu="sudo pacman -Syu" \
    Rsn="sudo pacman -Rsn" \
    pacman="sudo pacman"

Ss() { echo "$@" | xargs pacman -Ss }

n() { echo "$@" | node -p }

nn() { echo "const fs=require('fs');const path=require('path');$@" | node -p }

[ -d "$HOME/.TinyTeX/bin/x86_64-linux" ] && export PATH="$PATH:$HOME/.TinyTeX/bin/x86_64-linux"
