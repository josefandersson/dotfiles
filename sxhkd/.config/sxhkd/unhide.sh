#!/bin/bash

# If one hidden window, unhide it
# If multiple, prompt for which ones (one, many or all) to unhide

HIDN=$(bspc query -N -n .hidden)
C=$(echo "$HIDN" | wc -l)

[[ "$C" -eq 0 ]] && exit
[[ "$C" -eq 1 ]] && bspc node $HIDN -g hidden=off --focus && exit

LC=0
HIDN_ROFI="ALL"
for id in $HIDN; do
    WN=$(xdotool getwindowname $id)
    if [ -n "$WN" ]; then
        WG=$(xdotool getwindowgeometry --shell $id | tail -n 1 | cut -d'=' -f2)
        ((LC=LC+1))
        HIDN_ROFI+=$'\n'"$LC|${WG} $WN"
    fi
done

RES=$(echo "$HIDN_ROFI" | rofi -dmenu -i -p Unhide -no-custom -multi-select)
[ -z "$RES" ] && exit
[ "$(echo "$RES" | head -n 1)" = "ALL" ] && echo "$HIDN" | parallel bspc node {} -g hidden=off --focus && exit

RES=$(echo "$RES" | cut -d'|' -f1)
IDS=""
for i in $RES; do
    ID=$(echo "$HIDN" | tail -n+"$i" | head -n1)
    bspc node $ID -g hidden=off --focus
done