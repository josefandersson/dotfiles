#!/bin/bash

msg() {
    [ -z "$3" ] && t=3000 || t="$3"
    dunstify -t $t -a Macro "$1" "$2" -r 817234;
}

script='sxhkd'

[ -n "$2" ] && script="$2"

case "$1" in
    menu)
        $BASH_SOURCE "$(prompt "Macro '$script'" 'Record macro' 'record' 'Play macro' 'play' 'Play without delays' 'rmdelays' 'Loop macro' 'loop' 'Edit macro' 'edit' 'Choose macro' 'choose')"
        ;;
    edit)
        kitty -e /usr/bin/nano "/home/nicolas/.local/share/macros/$script"
        $BASH_SOURCE menu "$script"
        ;;
    choose)
        $BASH_SOURCE menu "$(macro list | rofi -dmenu -i -p 'Choose macro' -no-custom)"
        ;;
    record)
        msg 'Recording' 'Stop recording with "Pause".' 300000
	    macro key 127 record "$script" \
            && msg 'Done' "Macro '$script' was recorded, play it with "super+shift+o"." \
            || msg 'Error' 'Macro failed to record.'
        ;;
    play)
        msg 'Playing' "Playing macro '$script' in recorded speed."
	    macro sleep .2 speed 1 play "$script" \
            && msg 'Done' "Macro '$script' was played." \
            || msg 'Error' "Macro '$script' failed to play."
        ;;
    rmdelays)
        msg 'Playing' "Playing macro '$script' without delays."
	    macro sleep .2 rmdelays "$script" play "$script" \
            && msg 'Done' "Macro '$script' was played." \
            || msg 'Error' "Macro '$script' failed to play."
        ;;
    loop)
        res=$(rofi -l 2 -dmenu -i -p "Loop times")
        [ -z "$res" ] && exit 1
        [[ "$res" -gt 100 ]] && msg 'Error' "Looping $res times is too much!" && exit 1
        msg 'Playing' "Looping macro '$script' $res time."
        sleep .2
        for (( i=0; i<$res; ++i)); do
            macro play "$script" || (msg 'Error' "Macro '$script' failed to play on ituration $i/$res." && exit 1)
        done
        msg 'Done' "Macro '$script' was played $res times."
        ;;
esac