default: zsh dunst imwheel xorg bin rofi picom mpv sxhkd

zsh:
	stow -R zsh

dunst:
	stow -R dunst

openbox:
	stow -R openbox

ranger:
	stow -R ranger

tint2:
	stow -R tint2

i3lock-color:
	stow -R i3lock-color

imwheel:
	stow -R imwheel

xorg:
	stow -R xorg

neofetch:
	stow -R neofetch

bin:
	stow -R bin

rofi:
	stow -R rofi

picom:
	stow -R picom

kitty:
	stow -R kitty

mpv:
	stow -R mpv

bspwm:
	stow -R bspwm

sxhkd:
	stow -R sxhkd

polybar:
	stow -R polybar

feh:
	stow -R feh

nano:
	stow -R nano

setup:
	echo "-v --target=${HOME}" > .stowrc
