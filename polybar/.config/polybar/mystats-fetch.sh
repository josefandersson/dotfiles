#!/bin/bash

# TODO: Use cache-time to cache?

DATA=$(curl -s "https://mystats.josefadventures.org/api/activity/$1" -b "$MYSTATS_SESSION_ID" | jq '.activity.entries[-1] | .started + " " + (.length | tostring)')
STARTED=$(date -d "$(echo $DATA | grep -Eo '[^"].* ')" '+%s000')
ENDED=$(( $STARTED + $(echo $DATA | grep -Eo ' .*[^"]') ))
SINCE=$(( $(date '+%s000') - $ENDED ))
AGO="$2 $(millistotext -u y,w,d,h,m,s,ms -s '' -d1 '' -n 2 $SINCE)" 
echo $AGO