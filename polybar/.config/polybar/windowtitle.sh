#!/bin/bash

# Prints the title of the current focused window on monitor, also listens for
# focused window to change title and prints that (eg when changing tabs in browser).
# Usage: windowtitle.sh <monitor name>
# Example: ./windowtitle.sh DVI-0

monitor="$(bspc query -M -m $1)"
oldtitle=""
waitingfortitle=0
clrdark="$(xrdb -query | grep color8 -m 1 | cut -d$'\t' -f2 || echo "#555")"

# desktop_focus <monitor_id> <desktop_id>
# node_focus <monitor_id> <desktop_id> <node_id>
cat <(bspc subscribe desktop_focus node_focus & xtitle -s) | while read -a msg
do
    case "${msg[0]}" in
        desktop_focus)
            [ "${msg[1]}" != "$monitor" ] && echo "%{F$clrdark}$oldtitle" && waitingfortitle=0 && continue
            echo ""
            waitingfortitle=1;;
        node_focus)
            [ "${msg[1]}" != "$monitor" ] && echo "%{F$clrdark}$oldtitle" && waitingfortitle=0 && continue
            oldtitle="$()";;
        *)
            [ $waitingfortitle -eq 0 ] && continue
            oldtitle="${msg[@]}"
            echo $oldtitle
    esac
done