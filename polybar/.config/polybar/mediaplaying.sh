#!/bin/bash

# Listens for media changes and prints the currently playing media.

playerctl metadata --format "{{status}}^{{playerName}}^{{artist}}^{{title}}" --follow 2> /dev/null | while read line
do
    echo $line | grep -E '^\(playerctl' && continue

    status="$(echo $line | cut -d^ -f1)"
    [ -z "$status" ] || [ "$status" == "Paused" ] || [ "$status" == "Stopped" ] \
        && echo "%{F$(xrdb -query | grep color8 -m 1 | cut -d$'\t' -f2 || echo "#555")}not playing" && continue

    player="$(echo $line | cut -d^ -f2)"
    artist="$(echo $line | cut -d^ -f3)"
    title="$(echo $line | cut -d^ -f4)"
    case "$player" in
        "chromium") str="$title";;
        "spotify") str="$artist - $title";;
    esac
    echo "$str"
done