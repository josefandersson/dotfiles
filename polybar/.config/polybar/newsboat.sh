#!/bin/bash

# Usage: script [force]
#   force=reload even if it's recently been reloaded

echo 📰 %{F$(xrdb-get color8)}none

if [ "$1" == "force" ]; then
    cache-time newsboat force
    updated=1
else
    updated=$(cache-time newsboat passed 1800 updated)
fi

[ $updated -eq 1 ] && newsboat -x reload

num=$(newsboat -x print-unread | cut -d' ' -f1)
[ $num -gt 0 ] && echo 📰 %{F$(xrdb-get color4)}$num || echo 📰 %{F$(xrdb-get color8)}none