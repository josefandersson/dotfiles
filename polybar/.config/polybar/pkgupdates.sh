#!/bin/bash

# From:
# https://github.com/polybar/polybar-scripts/tree/master/polybar-scripts/updates-pacman-aurhelper

CL=($(xrdb-get color4 color8))

echo "📦 %{F${CL[1]}}wait"

if ! updates_arch=$(checkupdates 2> /dev/null | wc -l ); then
    updates_arch=0
fi

if ! updates_aur=$(trizen -Su --aur --quiet | wc -l); then
    updates_aur=0
fi

updates=$(( "$updates_arch" + "$updates_aur" ))

if [ "$updates" -gt 0 ]; then
    STR="📦 "
    [ $updates_arch -gt 0 ] && STR+="%{F${CL[0]}}$updates_arch%{F${CL[1]}}|" || STR+="%{F${CL[1]}}$updates_arch|"
    [ $updates_aur -gt 0 ] && STR+="%{F${CL[0]}}$updates_aur" || STR+="%{F${CL[1]}}$updates_aur"
    echo $STR
else
    echo "📦 %{F${CL[1]}}none"
fi