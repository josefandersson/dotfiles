#!/bin/bash

# Kill existing bars
killall -q -9 polybar
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Spawn new bars
if [ -z "$MON2" ]; then
    env MON=$MON1 polybar laptop &
else
    env MON=$MON1 polybar first &
    env MON=$MON2 polybar second &
fi